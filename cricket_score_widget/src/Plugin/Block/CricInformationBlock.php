<?php

/**
 * @file
 * Contains \Drupal\cricket_score_widget\Plugin\Block\CricInformationBlock.
 */

namespace Drupal\cricket_score_widget\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;

/**
 * Provides a 'Cricket Score Information' block.
 *
 * @Block(
 *   id = "cricinfo_block",
 *   admin_label = @Translation("Cricket Score Information")
 * )
 */
class CricInformationBlock extends BlockBase{
  
  public function settings() {
    return array(
      'cricket_score_block_height' => '',
      'cricket_score_block_width' => '',
    );
  }
  
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access cric information');
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Plain text field for the width of the widget.
    $form['cricket_score_block_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 4,
      '#description' => t('Width of the Widget in Pixel. Minimium value is 210'),
      '#default_value' => isset($this->configuration['cricket_score_block_width']) ? $this->configuration['cricket_score_block_width'] : '',
    );

    // Plain text field for the height of the widget.
    $form['cricket_score_block_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 4,
      '#description' => t('Height of the Widget in Pixel. Minimium value is 175'),
      '#default_value' => isset($this->configuration['cricket_score_block_height']) ? $this->configuration['cricket_score_block_height'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    /*if (!is_numeric($form_state['values']['count'])) {
      form_set_error('count', $form_state, t('Count must be numeric'));
    }*/
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    else {
      $this->configuration['cricket_score_block_width'] = $form_state->getValue('cricket_score_block_width');
      $this->configuration['cricket_score_block_height'] = $form_state->getValue('cricket_score_block_height');
    }
	$width = $form_state->getValue('cricket_score_block_width');
		$height = $form_state->getValue('cricket_score_block_height');
		if ($width < 210) {
		  $width = 210;
		}
		if ($height < 175) {
		  $height = 175;
		}
		$this->setConfigurationValue('cricket_score_block_width', $width);
		$this->setConfigurationValue('cricket_score_block_height', $height);
  }
 
  /**
   * {@inheritdoc}
   */
  public function build() {    
  
  $build['children']= array(
        '#theme' => 'cricket_score_widget_block',
        '#width' => $this->configuration['cricket_score_block_width'],
        '#height' => $this->configuration['cricket_score_block_height'],
      );
  return $build;
  
  }
  
  
}
