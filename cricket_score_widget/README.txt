About "Cricket Score Widget"
=======================

Cricket Score Widget block is a simple module to add a block to your Drupal installation
to display the cricket score.

Installation
============

After you activated the module (at "admin/modules") you can put the block to
any region on your page (using "admin/structure/block").

Configuration
=============

The configuration is available after you added the block to your site. The
easiest way is to click the configure link next to the block.

You can configure two aspects of the block:

1. Width

The width of the widget in pixels. The minimum value should be 210.

The variable used to store the value is "cricket_score_widget_width"

2. Height

The width of the widget in pixels. The minimum value should be 175.

The variable used to store the value is "cricket_score_widget_height"
